# Object detection dataset

This repository contains all labeled images used for object detection in the "Detecting Road Surface Damage using Deep Learning" thesis. Moreover, it also contains all evaluation images with predicted bounding boxes by seven models.
